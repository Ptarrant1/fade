#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

export DEBIAN_FRONTEND=noninteractive

#update all the things
sudo apt update && sudo apt upgrade -y sudo && apt-get autoremove -y && sudo  apt-get autoclean -y 

#base needs for ppa stuff
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

#common sense software
sudo apt install -y curl net-tools dnsutils python python3-pip tcpdump wget python2 python-dev rsync

#build tools
sudo apt install -y git autoconf automake flex gcc g++ libssl-dev zlib1g-dev libxml2-dev dpkg-dev openssl patch bison


#ppa repos for our tools
sudo add-apt-repository -y ppa:linuxgndu/sqlitebrowser
sudo add-apt-repository -y ppa:gift/stable
sudo add-apt-repository -y ppa:agornostal/ulauncher

#new repo updates
sudo apt update

#ir tools
sudo apt install -y xxd bless exiftool nmap yara plaso-tools ulauncher sqlitebrowser dos2unix hashdeep htop meld netcat 
sudo apt install -y p7zip-full ssdeep chaosreader dnsrecon ewf-tools myrescue recoverjpeg unhide pev dwarfdump zip unzip 
sudo apt install -y binwalk magicrescue scalpel scrounge-ntfs

#ra-tarmount
sudo pip3 install ratarmount

#pdf-parser
git clone https://gitlab.com/kalilinux/packages/pdf-parser.git /tmp/pdfparser
sudo cp /tmp/pdfparser/pdf-parser.py /usr/bin/pdf-parser.py
chmod +x /usr/bin/pdf-parser.py

#pdfid
git clone https://gitlab.com/kalilinux/packages/pdfid.git /tmp/pdfid
sudo cp /tmp/pdfid/pdfid /usr/bin/pdfid.py
chmod +x /usr/bin/pdfid.py


#python2 pip
curl https://bootstrap.pypa.io/get-pip.py --output /tmp/get-pip.py
sudo python2 /tmp/get-pip.py


#volatility
sudo pip2 install pycrypto yara-python distorm3==3.4.4
git clone https://github.com/volatilityfoundation/volatility.git /tmp/volatility
cd /tmp/volatility
sudo python2 setup.py install

#build bulkextractor
git clone https://github.com/simsong/bulk_extractor.git /tmp/bulkextractor
cd /tmp/bulkextractor
bash ./bootstrap.sh \
 && ./configure \
 && make \
 && make install

#pdfid.py
wget http://didierstevens.com/files/software/pdfid_v0_2_7.zip -O /tmp/pdfid.zip
mkdir /tmp/pdfid
unzip -d /tmp/pdfid/ /tmp/pdfid.zip
sudo mv /tmp/pdfid/pdfid.py /usr/bin/pdfid.py
chmod +x /usr/bin/pdfid.py

#cyberchef
wget https://github.com/gchq/CyberChef/releases/download/v9.21.0/CyberChef_v9.21.0.zip -O /tmp/cyberchef.zip
unzip -d /home/${USER}/Desktop/cyberchef /tmp/cyberchef.zip 

#snapcraft
apt install -y snapd

#vscode
snap install --classic code 

#responder
sudo snap install xllmnrd