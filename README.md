# FADE
## Forensic Analyst DFIR Environment
The purpose of FADE is to transform your Ubuntu 20.04 desktop into a forensics machine.

## But Why?
While the SIFT workstation is still available, it's based on Ubuntu 16.04 and many of it's packages break when trying to upgrade. 
The SIFT team is working on another version, also based on Ubuntu 20.04 but it's very much in development and I got tired of waiting.

## CMD Tools:
* binwalk - tear apart binary files
* bulk_extractor - pulls file from various sources.
* curl
* dnsrecon - dnsrecon
* dnsutils - dig etc
* dos2unix - changes returns to unix (\r to \n)
* efw-tools - eye witness tools
* hashdeep - hasher
* htop - system monitoring app
* log2timeline / plaso - forensic timeline generation
* magicrescue - grabs files from block devices
* myrescue - recover files from damaged media
* net-tools - netstat etc
* netcat - utility for reading from and writing to network connections 
* nmap - network tool/scanner
* p7zip - 7zip support
* pdf-parser - find malicious pdfs
* pdfid - find malicious pdfs
* pev - pe forensic analyzer
* python2 - some tools need python2
* python3 
* ratarmount - mounts tar archives as ro device
* recoverjpeg - recover photos
* responder - traffic spoofer
* rsync - copy utility
* scalpel - carve files
* scrounge-ntfs - get data from ntfs drives
* ssdeep - hasher
* tcpdump - pcap creator
* unhide - root kit detector
* volatility (2.6.1) - memory analysis
* wget - web downloader


## Desktop Tools:
* bless - HEX editor
* chaosreader - pcap to html generator
* cyberchef - encode/decode multiple things
* guymager - create forensic images
* meld - diff viewer
* sqlitebrowser - read sqlite files
* ulauncher - ctl+space launcher
* vscode - code editor / ide
